# EzZiboInstallerPro

## Legal stuff
\
**USE AT OWN RISK!!!! I CAN NOT BE HELD RESPONSIBLE!!**

## General info
EzZibo is a small tool i made just to make installing Zibo and updating it a lot easier.\
Download the full zip and the latest update(no need for all updates) and tell the tool where they are.\
\
Beta testers are welcome please drop me a msg on discord.\
If you find bugs, please let me know as well on discord.

## Release info
**v10.3.2**\
- Minor fix in the update section to make sure temp folder is not there
- Fixed detection if the selected folder is a valid zibo install by looking in the acf file when multiple installations.
\
**v10.3.1**\
-Minor fix in .acf file search\
\
**v10.3.0**\
-Fixed new versioning in version.txt.\
-fixed new version check in .acf file.\
-Fixed rename of full zip extraction when name was the same as expected.\
-Fixed flow error when you tried to install zibo twice in the same session.\
\
**v10.2.3**\
-Fixed update file size for 4.00rc3.8.\
\
**v10.2.2**\
-Fixed bug where if only 1 zibo was installed an error occured.\
\
**v10.2.1**\
-Reworked the Zibo detection code with some traps to make sure we have a valid Zibo.\
\
**v10.2.0**\
-Changed the backup code to use 7-Zip, not the build in PowerShell compress fuction which had a memory leak.\
 and will not let me lower compression rate to speed things up.\
-First attempt to change the Zibo detection code to make sure there is no clash with the level up nor the default Laminar 737.\
 This code needs to be changed again to have it trap any mistakes.\
\
**v10.1.1**\
-Bugfix in the backup procedure fixed (Thanx to ChezHJ for finding it)\
-Updated the manual part a bit.\
\
**v10.1.0**\
-Split releases to a pro and non pro version.\
 Non pro works for 1 Zibo install and has no fancy features. This is for the "normal" people.\
 Pro version will have features like multiple installs of Zibo, and probably a backup function.\

## below this, its all the non pro version!
\
**v1.0.1**\
-Put a heavy search query  for finding the Zibo into a variable array so i do not have to execute it 6 times.\
\
**v1.0**\
-Formatting of in app release notes changed.\
-Added a trap to catch if more then 1 zibo is installed.\
-Changed the way a zibo folder is detected to prevent not finding all if there are multiple\
-Added checking for X-Plane mismatch in full and updates\
\
**v0.9d**\
-Fixed bug: update to same verion is possible.\
-Added release notes to help menu.\
-Creation of zibo folder in aircraft is now with a capital Z.\
-Added conformation to succesful install.\
-Added conformation to succesful update.\
-Reverted release info, newest version is now on top\
\
**v0.9c**\
-Included a check if xPlane is running.\
-Small rewrite of zibofolder detect code to enhance performance a lot.\
\
**v0.9b**\
-Many bugfixes and added checks.\
-This is a point of something that should work.\
\
**v0.9a**\
-Initial Script.


## Manual
It is simple.....\
Push the buttons to do stuff....\
This will work either for XP11 or XP12 installations...\
\
**Install Zibo without updates:**\
-Click the "Set Zibo full zip file"\
Browse to the full zipfile and click ok.\
-Click "Set X-Plane Path"\
Browse to your base xplane path and click ok.\
-Click "Install Full Zibo"\
Wait a few minutes and you are done.\
\
**Install Zibo with updates:**\
-Click the "Set Zibo full zip file"\
Browse to the full zipfile and click ok.\
-Click "Set Zibo update file"\
Browse to the update zipfile and click ok\
-Click "Set X-Plane Path"\
Browse to your base xplane path and click ok.\
-Click "Install + Update Zibo"\
Wait a few minutes and you are done.\
\
**Update an existing Zibo installation**\
-Click "Set Zibo update file"\
Browse to the update zipfile and click ok.\
-Click "Set X-Plane Path"\
Browse to your base xplane path and click ok.\
-Click "Update Zibo"\
Wait a few minutes and you are done.\
\
If you find a bug, or want a feature added, do let me know!
\
Grtz,\
Alex_NL74
